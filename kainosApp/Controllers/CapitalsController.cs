﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using kainosApp.Models;

namespace kainosApp.Controllers
{
    public class CapitalsController : Controller
    {
        private readonly CapitalDbContext db = new CapitalDbContext();

        // GET: Capitals
        public async Task<ActionResult> Index()
        {
            var cap = await db.Capitals.ToListAsync();
            return View(cap);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public async Task<ActionResult> Index(HttpPostedFileBase file)
        {
            string path = null;
            var capitals = new List<Capital>();
            try
            {
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("~/App_data/upload/"), fileName);
                    file.SaveAs(path);
                    var csv = new CsvReader(new StreamReader(path));
                    csv.Configuration.Delimiter = ",";
                    csv.Configuration.HasHeaderRecord = false;
                    var CapitalsList = csv.GetRecords<CapitalToRead>();

                    foreach (var capital in CapitalsList)
                    {
                        var capitalToDisplay = new Capital();

                        capitalToDisplay.Id = int.Parse(capital.Id);
                        capitalToDisplay.Date = DateTime.Parse(capital.Date);
                        capitalToDisplay.Value = decimal.Parse(capital.Value.Replace('.', ','));
                        Console.WriteLine(capitalToDisplay);
                        capitals.Add(capitalToDisplay);
                    }

                    db.Capitals.RemoveRange(db.Capitals);

                    await db.SaveChangesAsync();
                    db.Capitals.AddRange(capitals);

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> CuttedIndex(DateTime startDate, DateTime endDate)
        {
            var capitals = await db.Capitals.Where(c => c.Date >= startDate && c.Date <= endDate).ToListAsync();
            return View("Index", capitals);
        }

        public async Task<ActionResult> Compare()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Compare(DateTime startDate, DateTime endDate, decimal percentage)
        {
            var locate = new List<Capital>();
            var capitals = db.Capitals.Where(c => c.Date >= startDate && c.Date <= endDate).ToList();
            if (capitals.Count < 1 || capitals == null)
            {
                return RedirectToAction("Compare");
            }
            capitals = capitals.OrderBy(c => c.Date).ToList();
            locate.Add(capitals[0]);
            percentage = percentage / 100 / 365;
            var i = 0;
            for (var day = startDate.Date.AddDays(1); day.Date < endDate.Date; day = day.AddDays(1))
            {
                i++;
                var loc = new Capital();
                loc.Date = day;
                loc.Value = locate[i - 1].Value + locate[i - 1].Value * percentage;
                locate.Add(loc);
            }
            locate = locate.OrderBy(c => c.Date).ToList();
            var model = new CapitalWithDates();
            model.Locate = locate;
            model.Capitals = capitals;
            model.percentage = percentage;
            return View(model);
        }
    }
}
