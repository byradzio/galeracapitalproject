﻿
google.charts.load("current", { packages: ["corechart"] });
google.charts.setOnLoadCallback(drawChart);

function drawChart() {


    var data = new google.visualization.DataTable();
    data.addColumn("date", "Date");
    data.addColumn("number", "Value");
    model.forEach(function (entry) {
        var date = new Date(parseInt(entry.Date.substr(6)));
        var dateString = date.getFullYear().toString() + "," + (date.getMonth() + 1).toString() + "," + date.getDate().toString();
        data.addRow([new Date(dateString), entry.Value]);
    });
    var options = {
        tilte: "Capital",
        height: 600
    };
    var chart = new google.visualization.LineChart(document.getElementById("myChart"));
    chart.draw(data, options);
}