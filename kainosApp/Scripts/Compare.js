﻿
google.charts.load("current", { packages: ["corechart", "timeline"] });
google.charts.setOnLoadCallback(drawChart);


function drawChart() {
    if (model != null) {
        var data = new google.visualization.DataTable();
        data.addColumn("date", "Date");
        data.addColumn("number", "Capital");

        model.Capitals.forEach(function (entry) {
            var date = new Date(parseInt(entry.Date.substr(6)));
            var dateString = date.getFullYear().toString() + "," + (date.getMonth() + 1).toString() + "," + date.getDate().toString();
            data.addRow([new Date(dateString), entry.Value]);
        });
        var data2 = new google.visualization.DataTable();

        data2.addColumn("date", "Date");
        data2.addColumn("number", "Locate");
        model.Locate.forEach(function (entry) {
            var date2 = new Date(parseInt(entry.Date.substr(6)));

            var dateString = date2.getFullYear().toString() + "," + (date2.getMonth() + 1).toString() + "," + date2.getDate().toString();

            data2.addRow([new Date(dateString), entry.Value]);
        });

        var joinedData = google.visualization.data.join(data, data2, "left",
        [[0, 0]], [1], [1]);

        joinedData.sort({ column: 0, desc: true });
        var options = {
            tilte: "Fundation",
            height: 600
        };
        var chart = new google.visualization.LineChart(document.getElementById("myCompareChart"));

        chart.draw(joinedData, options);
    }
}

