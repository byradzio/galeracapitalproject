﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace kainosApp.Models
{
    public class Capital
    {
        public int Id { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Date { get; set; }

        public decimal Value { get; set; }
    }

    public class CapitalToRead
    {
        public string Id { get; set; }
        public string Date { get; set; }
        public string Value { get; set; }
    }

    public class CapitalWithDates
    {
        public int Id { get; set; }

        public List<Capital> Capitals { get; set; }

        public decimal percentage { get; set; }
        public List<Capital> Locate { get; set; }
    }

    public class CapitalDbContext : DbContext
    {
        public CapitalDbContext() : base("DefaultConnection")
        {
        }
        public DbSet<Capital> Capitals { get; set; }
    }
}
